package com.androidstudioisbad.screenpusher

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_calculator.*
import net.objecthunter.exp4j.ExpressionBuilder

class Calculator : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculator)

        var input1 = ""

        text_result!!.showSoftInputOnFocus = false
        btn_ce.setOnClickListener {view ->
            text_result.setText("")
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        btn_c.setOnClickListener {

        }

        btn_equal.setOnClickListener {
            val expression = ExpressionBuilder(input1).build()
            val result = expression.evaluate()
            text_result.setText(result.toString(), TextView.BufferType.NORMAL)
        }

        btn_add.setOnClickListener {
            input1 += '+'
            text_result.setText(input1.toString(), TextView.BufferType.NORMAL)
        }

        btn_minus.setOnClickListener {
            input1 += '-'
            text_result.setText(input1.toString(), TextView.BufferType.NORMAL)
        }

        btn_multi.setOnClickListener {
            input1 += '*'
            text_result.setText(input1.toString(), TextView.BufferType.NORMAL)
        }

        btn_div.setOnClickListener {
            input1 += '/'
            text_result.setText(input1.toString(), TextView.BufferType.NORMAL)
        }

        btn_num0.setOnClickListener {
            input1 += '0'
            text_result.setText(input1.toString(), TextView.BufferType.NORMAL)
        }

        btn_num1.setOnClickListener {
            input1 += '1'
            text_result.setText(input1.toString(), TextView.BufferType.NORMAL)
        }

        btn_num2.setOnClickListener {
            input1 += '2'
            text_result.setText(input1.toString(), TextView.BufferType.NORMAL)
        }

        btn_num3.setOnClickListener {
            input1 += '3'
            text_result.setText(input1.toString(), TextView.BufferType.NORMAL)
        }

        btn_num4.setOnClickListener {
            input1 += '4'
            text_result.setText(input1.toString(), TextView.BufferType.NORMAL)
        }

        btn_num5.setOnClickListener {
            input1 += '5'
            text_result.setText(input1.toString(), TextView.BufferType.NORMAL)
        }

        btn_num6.setOnClickListener {
            input1 += '6'
            text_result.setText(input1.toString(), TextView.BufferType.NORMAL)
        }

        btn_num7.setOnClickListener {
            input1 += '7'
            text_result.setText(input1.toString(), TextView.BufferType.NORMAL)
        }

        btn_num8.setOnClickListener {
            input1 += '8'
            text_result.setText(input1.toString(), TextView.BufferType.NORMAL)
        }

        btn_num9.setOnClickListener {
            input1 += '9'
            text_result.setText(input1.toString(), TextView.BufferType.NORMAL)
        }




    }


}
